#!/usr/bin/env zsh

NUM=20
MAXTERM=14
STEP=4
GENERATOR=./bin/GraphGenerator.py
DATA_DIR=./data

mkdir -p ${DATA_DIR}

types=("cu" "cr" "r0.5u" "r0.5r" "mu" "mr")

for tp in "${types[@]}"
do
	for v in {5..30..5}
	do
		n=$(($v**2))
		min=$(( $v > $MAXTERM ? $MAXTERM : $v ))
		for t in {2..$min..$STEP}
		do
			for i in {1..${NUM}}
			do
			nice -19 $GENERATOR -v $n -t $t --type $tp > ${DATA_DIR}/${tp}_${n}_${t}_${i}.net
			done
		done
	done
done

echo "DONE!!"
