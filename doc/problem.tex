\documentclass[a4paper]{article}
\usepackage[top=30truemm,bottom=30truemm,left=20truemm,right=30truemm]{geometry}
\usepackage[dvipdfmx]{graphicx}
\usepackage{listings}
\usepackage{amsmath}
\usepackage{mathtools}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
\title{Steiner Tree Problem}
\author{Otsuki Hitoshi}
\date{\today}
\begin{document}
\maketitle
\section{Formulation with Integer Programming}
\subsection{Naive Formulation}
\begin{align*}
	&\text{min}& & C\\
	&\text{subject to}	& &x(\delta(W)) \ge 1, \forall W\subset V, W\cap T_{i}\neq \phi, \left(V\setminus W\right)\cap T_{i}\neq\phi\\
	&				  	& &x_{ie} = \left\{0,1\right\}\\
	&					& &\forall U\subset V, |E(U)|\le |U|-1\\
	&					& &i = \left\{1,\cdots,N\right\}\\
	&					& &0\le\sum_{i}^{N}\mathbf{bw}_{i}x_{ie}\le c_{e}
\end{align*}
The number of $W$ is:
\begin{align*}
	\sum_{i=1}^{|T|}\sum_{j=1}^{\floor*{\frac{|T_{i}|}{2}}} {|T_{i}| \choose j} 2^{|V|-|T_{i}|}
\end{align*}
\subsection{Sophisticated Formulation}
The formulation above requires exponential constraints. Therefore, we can use another formulation as shown below:
\begin{align}
	&\text{min}& & C\label{eq:2.1}\\
	&\text{subject to}	& &z_{t}\in Z_{t}\\
	&                 & &\sum_{i\in V^{-}(j)}x(t)_{ij}\le 1, \forall j\in V(\overrightarrow{G})-\left\{z_{t}\right\}\label{eq:2.2}\\
	&				  	& &n\sum_{i\in V^{-}(j)}x(t)_{ij}\ge u(t)_{j}+1, \forall j\in V(\overrightarrow{G})-\left\{z_{t}\right\}\label{eq:2.3}\\
	&					& &(n+1)\sum_{i\in V^{-}(j)}x(t)_{ij}\le n(u(t)_{j}+1), \forall j\in V(\overrightarrow{G})-\left\{z_{t}\right\}\label{eq:2.4}\\
	&					& &1-n(1-x(t)_{ij})\le u(t)_{j}-u(t)_{i}\le 1+n(1-x(t)_{ij}), \forall ij\in E(\overrightarrow{G})\label{eq:2.5}\\
	&					& &x(t)_{ij}\in\left\{0,1\right\}, \forall ij\in E(\overrightarrow{G})\label{eq:2.6}\\
	&					& &u(t)_{z_{t}}=0, u(t)_{i}\ge 0, \forall i\in Z_{t}-\left\{z_{t}\right\}\label{eq:2.7}\\
	&					& &t\in\left\{1,\cdots,T\right\}\label{eq:2.9}\\
	&					& &0\le\sum_{t}^{T}bw_{t}(x(t)_{ij}+x(t)_{ji})\le c_{ij}, \forall ij\in E(\overrightarrow{G}), i<j\label{eq:2.10}
\end{align}
\begin{table}
	\centering
	\begin{tabular}{|c|c|}\hline
		Constraints & \# of Constraints\\\hline
		\ref{eq:2.2},\ref{eq:2.3},\ref{eq:2.4}& $n-1$\\\hline
		\ref{eq:2.5},\ref{eq:2.6}& $2m$\\\hline
		\ref{eq:2.7}&$|Z_{t}|-1$\\\hline
		\ref{eq:2.10}&$\frac{(m-1)m}{2}$\\\hline
	\end{tabular}
	\caption{Number of constraints}
	\label{tab:const_table}
\end{table}
By Table \ref{tab:const_table}, the total number of constraints for steiner tree problem is,
\begin{align}
	|C| &= \sum_{t=1}^{T}\left( \left( n-1 \right)\cdot 3 + 2m\cdot 2 + |Z_{t}| - 1 \right) + \frac{m(m-1)}{2}\\
	&= \sum_{t=1}^{T}\left( 4m + 3n + |Z_{t}| - 4 \right) + \frac{m(m-1)}{2}\\
	&=\left( 4m + 3n -4 \right)\cdot T + \frac{(m-1)m}{2} + \sum_{t=1}^{T}|Z_{t}|\label{eq:3.3}
\end{align}
\ref{eq:3.3} shows the number of constraints is polynomial with respect to $m,n,p_{t},T$.
\subsection{Handling of multiple edges between two nodes}
The number of constraints is decreased to polynomial. But there is a one big problem we have to address. Even if there are mulitiple edges between two nodes the formulation above will allow only one edge. To enable this, we introduce $x(t)_{ijk}$ denoting the  $k$th edge from $i$ to $j$.
\begin{align}
	&\text{min}& & C\label{eq:3.1}\\
	&\text{subject to}	& &\sum_{i\in V^{-}(j)}\sum_{k=1}^{|E_{ij}|}x(t)_{ijk}\le 1, \forall j\in V(\overrightarrow{G})-\left\{z_{t}\right\}\label{eq:3.2}\\
	&				  	& &n\sum_{i\in V^{-}(j)}\sum_{k=1}^{|E_{ij}|}x(t)_{ijk}\ge u(t)_{j}+1, \forall j\in V(\overrightarrow{G})-\left\{z_{t}\right\}\label{eq:3.3}\\
	&					& &(n+1)\sum_{i\in V^{-}(j)}\sum_{k=1}^{|E_{ij}|}x(t)_{ijk}\le n(u(t)_{j}+1), \forall j\in V(\overrightarrow{G})-\left\{z_{t}\right\}\label{eq:3.4}\\
	&					& &1-n(1-x(t)_{ijk})\le u(t)_{j}-u(t)_{i}\le 1+n(1-x(t)_{ijk}), \forall ij\in E(\overrightarrow{G}), \forall k\in\left\{ 1,\cdots,|E_{ij}| \right\}\label{eq:3.5}\\
	&					& &x(t)_{ijk}\in\left\{0,1\right\}, \forall ij\in E(\overrightarrow{G}), \forall k\in\left\{ 1,\cdots,|E_{ij}| \right\}\label{eq:3.6}\\
	&					& &u(t)_{z_{t}}=0, u(t)_{i}\ge 0, \forall i\in Z_{t}-\left\{z_{t}\right\}\label{eq:3.7}\\
	&					& &z_{t}\in Z_{t}\label{eq:3.8}\\
	&					& &t\in\left\{1,\cdots,T\right\}\label{eq:3.9}\\
	&					& &0\le\sum_{t}^{T}bw_{t}(x(t)_{ijk}+x(t)_{jik})\le c_{ijk}, \forall ij\in E(\overrightarrow{G}), i<j, \forall k\in\left\{ 1,\cdots,|E_{ij}| \right\}\label{eq:3.10}
\end{align}
This formulation does not change the number of variables or constraints. It just make it possible to handle with multiple edges beween two nodes.
\end{document}
