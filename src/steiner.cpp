#include "steiner.h"
#include <sstream>
#include "scip_exception.hpp"

using namespace std;
using namespace steiner;

/* constructor */
steiner::SteinerTreeSolver::SteinerTreeSolver(char *filename){
	st = new SteinerTrees(filename);

	// initialize scip
	SCIP_CALL_EXC( SCIPcreate(& _scip) );

	// load default plugins linke separators, heuristics, etc.
	SCIP_CALL_EXC( SCIPincludeDefaultPlugins(_scip) );

	// disable scip output to stdout
	SCIP_CALL_EXC( SCIPsetMessagehdlr(_scip, NULL) );

	// create an empty problem
	SCIP_CALL_EXC( SCIPcreateProb(_scip, "steiner", NULL, NULL, NULL, NULL, NULL, NULL, NULL) );

	// set the objective sense to maximize, default is minimize
	SCIP_CALL_EXC( SCIPsetObjsense(_scip, SCIP_OBJSENSE_MINIMIZE) );

	_x.resize(st->T());
	_u.resize(st->T());
	_root.resize(st->T());

	for(int i = 0; i < st->T();++i){
		_u[i].resize(st->graph.V()+1);
	}

	// create a binary variable x(t)_{ij}
	ostringstream namebuf;

	for(int t = 0; t < st->T();++t){
		for (int i = 1; i <= st->graph.V(); ++i){
			for(map<int, vector<FlowEdge> >::iterator it = st->graph.adj(i).begin(); it != st->graph.adj(i).end(); ++it){
				for(unsigned int k = 0; k < it->second.size(); k++ ){
					SCIP_VAR * var;
					namebuf.str("");
					namebuf << "x(" << t << ")"<< "_{" << i << "," << it->first << "," << k+1 << "}";
					// create the SCIP_VAR object
					SCIP_CALL_EXC( SCIPcreateVar(_scip, & var, namebuf.str().c_str(), 0.0, 1.0, 0.0, SCIP_VARTYPE_BINARY, TRUE, FALSE, NULL, NULL, NULL, NULL, NULL) );

					// add the SCIP_VAR object to the scip problem
					SCIP_CALL_EXC( SCIPaddVar(_scip, var) );

					// storing the SCIP_VAR pointer for later access
					_x[t][i][it->first].push_back(var);
				}
			}
			SCIP_VAR * var;
			namebuf.str("");
			namebuf << "u(" << t << ")" << "_{" <<  i <<"}";
			// create the SCIP_VAR object
			if(i == *(st->getTerminals(t).begin())){
				SCIP_CALL_EXC( SCIPcreateVar(_scip, & var, namebuf.str().c_str(), 0.0, 0.0, 0.0, SCIP_VARTYPE_INTEGER, TRUE, FALSE, NULL, NULL, NULL, NULL, NULL) );
				_root[t] = i;
			}
			else{
				SCIP_CALL_EXC( SCIPcreateVar(_scip, & var, namebuf.str().c_str(), -SCIPinfinity(_scip), SCIPinfinity(_scip), 0.0, SCIP_VARTYPE_INTEGER, TRUE, FALSE, NULL, NULL, NULL, NULL, NULL) );
			}
			// add the SCIP_VAR object to the scip problem
			SCIP_CALL_EXC( SCIPaddVar(_scip, var) );
			// storing the SCIP_VAR pointer for later access
			_u[t][i] = var;
		}
	}
	cerr<<"Variables are created"<<endl;

	// create constraints
	for(int t = 0; t < st->T();++t){
		for(int j = 1;j <= st->graph.V();j++){
			if(j==_root[t]){
				continue;
			}
			SCIP_CONS *cons;
			namebuf.str("");
			namebuf<<"indeg_"<<t<<"_"<<j;
			// create SCIP_CONS object
			// indegree of each vertex except for the root muct be at most 1.
			SCIP_CALL_EXC( SCIPcreateConsLinear(_scip, & cons, namebuf.str().c_str(), 0, NULL, NULL, 0.0, 1.0,TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE) );
			for(pair<const int,vector<FlowEdge> > & p: st->graph.adj(j)){
				for(unsigned int k = 0; k < p.second.size();++k){
					SCIP_CALL_EXC( SCIPaddCoefLinear(_scip, cons, _x[t][p.first][j][k], 1.0) );
				}
			}
			// add the constraint to scip
			SCIP_CALL_EXC( SCIPaddCons(_scip, cons) );
			// store the constraint for later on
			_cons.push_back(cons);
		}

		for(int j = 1;j <= st->graph.V();j++){
			if(j==_root[t]){
				continue;
			}
			SCIP_CONS *cons;
			namebuf.str("");
			namebuf<<"dismax_"<<t<<"_"<<j;
			// create SCIP_CONS object
			// (n+1)\sum_{i\in V^{-}(j)}x(t)_{ij}\le n(u(t)_{j}+1)
			SCIP_CALL_EXC( SCIPcreateConsLinear(_scip, & cons, namebuf.str().c_str(), 0, NULL, NULL, 1.0, SCIPinfinity(_scip), TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE));
			// add the vars belonging to field in this row to the constraint
			for(pair<const int,vector<FlowEdge> > & p: st->graph.adj(j)){
				for(unsigned int k = 0; k < p.second.size();++k){
					SCIP_CALL_EXC( SCIPaddCoefLinear(_scip, cons, _x[t][p.first][j][k], (double)st->graph.V()));
				}
			}
			SCIP_CALL_EXC( SCIPaddCoefLinear(_scip, cons, _u[t][j], -1.0));
			// add the constraint to scip
			SCIP_CALL_EXC( SCIPaddCons(_scip, cons) );
			// store the constraint for later on
			_cons.push_back(cons);
		}

		for(int j = 1;j <= st->graph.V();j++){
			if(j==_root[t]){
				continue;
			}
			SCIP_CONS *cons;
			namebuf.str("");
			namebuf<<"dismin_"<<t<<"_"<<j;
			// create SCIP_CONS object
			// (n+1)\sum_{i\in V^{-}(j)}x(t)_{ij}\le n(u(t)_{j}+1)
			SCIP_CALL_EXC( SCIPcreateConsLinear(_scip, & cons, namebuf.str().c_str(), 0, NULL, NULL, -SCIPinfinity(_scip), (double)st->graph.V(), TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE));
			for(pair<const int,vector<FlowEdge> > & p: st->graph.adj(j)){
				for(unsigned int k = 0; k < p.second.size();++k){
					SCIP_CALL_EXC( SCIPaddCoefLinear(_scip, cons, _x[t][p.first][j][k], st->graph.V()+1.0));
				}
			}
			SCIP_CALL_EXC( SCIPaddCoefLinear(_scip, cons, _u[t][j], -1.0*st->graph.V()));
			// add the constraint to scip
			SCIP_CALL_EXC( SCIPaddCons(_scip, cons) );
			// store the constraint for later on
			_cons.push_back(cons);
		}

		for(int j = 1;j <= st->graph.V();j++){
			for(pair<const int,vector<FlowEdge> > & p: st->graph.adj(j)){
				for(unsigned int k = 0; k < p.second.size();++k){
					SCIP_CONS *cons1,*cons2;
					namebuf.str("");
					namebuf<<"distdiff_"<<t<<"_"<<j<<"_"<<p.first<<"_1";
					// create SCIP_CONS object
					SCIP_CALL_EXC( SCIPcreateConsLinear(_scip, & cons1, namebuf.str().c_str(), 0, NULL, NULL, 1.0 - st->graph.V(), SCIPinfinity(_scip),
								TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE) );

					SCIP_CALL_EXC( SCIPaddCoefLinear(_scip, cons1, _x[t][j][p.first][k], -1.0*st->graph.V()));
					SCIP_CALL_EXC( SCIPaddCoefLinear(_scip, cons1, _u[t][p.first], 1.0));
					SCIP_CALL_EXC( SCIPaddCoefLinear(_scip, cons1, _u[t][j], -1.0));
					// add the constraint to scip
					SCIP_CALL_EXC( SCIPaddCons(_scip, cons1) );
					// store the constraint for later on
					_cons.push_back(cons1);
					namebuf.str("");
					namebuf<<"distdiff_"<<t<<"_"<<j<<"_"<<p.first<<"_2";
					// create SCIP_CONS object
					SCIP_CALL_EXC( SCIPcreateConsLinear(_scip, & cons2, namebuf.str().c_str(), 0, NULL, NULL, -SCIPinfinity(_scip), 1.0 + st->graph.V(), TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE) );

					SCIP_CALL_EXC( SCIPaddCoefLinear(_scip, cons2, _x[t][j][p.first][k], (double)st->graph.V()));
					SCIP_CALL_EXC( SCIPaddCoefLinear(_scip, cons2, _u[t][p.first], 1.0));
					SCIP_CALL_EXC( SCIPaddCoefLinear(_scip, cons2, _u[t][j], -1.0));
					// add the constraint to scip
					SCIP_CALL_EXC( SCIPaddCons(_scip, cons2) );
					// store the constraint for later on
					_cons.push_back(cons2);
				}
			}
		}

		for(int terminal: st->getTerminals(t)){
			if(terminal==_root[t]){
				continue;
			}
			SCIP_CONS *cons;
			namebuf.str("");
			namebuf<<"dis_positive_"<<t<<","<<terminal;
			// create SCIP_CONS object
			// u_{i} \ge 0
			SCIP_CALL_EXC( SCIPcreateConsLinear(_scip, & cons, namebuf.str().c_str(), 0, NULL, NULL, 0.0, SCIPinfinity(_scip), TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE));
			SCIP_CALL_EXC( SCIPaddCoefLinear(_scip, cons, _u[t][terminal], 1.0));
			// add the constraint to scip
			SCIP_CALL_EXC( SCIPaddCons(_scip, cons) );
			// store the constraint for later on
			_cons.push_back(cons);
		} 
	}

	for(int i = 1;i <= st->graph.V();i++){
		for(pair<const int, vector<FlowEdge> > & p: st->graph.adj(i)){
			for(unsigned int k = 0;k<p.second.size();++k){
				if(i>=p.first){
					continue;	
				}
				SCIP_CONS *cons;
				namebuf.str("");
				namebuf<<"cap_"<<i<<","<<p.first<<","<<k+1;
				// create SCIP_CONS object
				// capacity constraint
				SCIP_CALL_EXC( SCIPcreateConsLinear(_scip, & cons, namebuf.str().c_str(), 0, NULL, NULL, 0.0, (double)p.second[k].capacity(), TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE));
				for(int t = 0;t<st->T();++t){
					SCIP_CALL_EXC( SCIPaddCoefLinear(_scip, cons, _x[t][i][p.first][k], (double)(st->getBandWidth(t))));
					SCIP_CALL_EXC( SCIPaddCoefLinear(_scip, cons, _x[t][p.first][i][k], (double)(st->getBandWidth(t))));
				}
				// add the constraint to scip
				SCIP_CALL_EXC( SCIPaddCons(_scip, cons) );
				// store the constraint for later on
				_cons.push_back(cons);
			}

		}
	}
}


/* display the solution */
void steiner::SteinerTreeSolver::disp(std::ostream& out){
	// get the best found solution from scip
	SCIP_SOL* sol = SCIPgetBestSol(_scip);
	out << "solution for Steiner tree problem:" << endl << endl;
	if (sol == NULL){
		out << "No Solution found." << endl;
		return;
	}
	for (int t = 0; t < st->T(); ++t){
		for(map<int,map<int,vector<SCIP_VAR*> > >::iterator it1 = _x[t].begin(); it1!=_x[t].end(); ++it1){
			for(map<int,vector<SCIP_VAR* > >::iterator it2 = _x[t][it1->first].begin();it2!=_x[t][it1->first].end();++it2){
				for(unsigned int k = 0;k < _x[t][it1->first][it2->first].size();++k){
					out<<_x[t][it1->first][it2->first][k]->name<<"="<<SCIPgetSolVal(_scip, sol, _x[t][it1->first][it2->first][k])<<endl;
				}
			}
		}
		for(unsigned i = 1;i<_u[t].size();++i){
			out<<_u[t][i]->name<<"="<<SCIPgetSolVal(_scip, sol, _u[t][i])<<endl;
		}
	}
	out << endl;
}


/* destructor */
steiner::SteinerTreeSolver::~SteinerTreeSolver(void){
	// since the SCIPcreateVar captures all variables, we have to release them now
	for (int t = 0; t < st->T(); ++t){
		for(map<int,map<int,vector<SCIP_VAR*> > >::iterator it1 = _x[t].begin(); it1!=_x[t].end(); ++it1){
			for(map<int,vector<SCIP_VAR* > >::iterator it2 = _x[t][it1->first].begin();it2!=_x[t][it1->first].end();++it2){
				for(unsigned int k = 0;k < _x[t][it1->first][it2->first].size();++k){
					SCIP_CALL_EXC( SCIPreleaseVar(_scip, &_x[t][it1->first][it2->first][k]) );
				}
			}
		}
		for(unsigned i = 2;i<_u[t].size();++i){
			SCIP_CALL_EXC( SCIPreleaseVar(_scip, &_u[t][i]) );
		}
	}

	_x.clear();
	_u.clear();
	// the same for the constraints
	for (vector<SCIP_CONS *>::size_type i = 0; i < _cons.size(); ++i){
		SCIP_CALL_EXC( SCIPreleaseCons(_scip, &_cons[i]) );
	}
	_cons.clear();
	// after releasing all vars and cons we can free the scip problem
	// remember this has allways to be the last call to scip
	SCIP_CALL_EXC( SCIPfree( & _scip) );
}

/* solve the steiner tree problem */
void SteinerTreeSolver::solve(void){
	// this tells scip to start the solution process
	SCIP_CALL_EXC( SCIPsolve(_scip) );
}
