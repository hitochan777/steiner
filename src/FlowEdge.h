#ifndef FLOW_EDGE_H
#define FLOW_EDGE_H

#include <iostream>
#include <vector>
#include <string>
#include <stdexcept>

class FlowEdge{
	private:
		int _v; //from
		int _w; //to
		int _capacity;
	public:
		FlowEdge(int v, int w, int capacity);
		int from();
		int to();
		int capacity();
		int other(int vertex);
		~FlowEdge();
};

#endif
