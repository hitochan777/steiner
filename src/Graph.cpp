#include "Graph.h"

Graph::Graph(){}

Graph::Graph(int V){
	if(V<0){
		throw std::invalid_argument("Number of vertices cannot be negative.");
	}
	_V = V;
	_E = 0;
	_adj.resize(_V+1);
}

Graph::Graph(char* filename){
	std::ifstream ifs; 
	int E;
	ifs.open(filename,std::ios::in);
	if(ifs.fail()){
		throw std::invalid_argument("Failed to open "+std::string(filename));
	}
	ifs >> _V >> E;
	for(int i = 0,v,w,c;i<E;++i){
		ifs >> v >> w >> c;
		FlowEdge f1(v,w,c);
		addEdge(f1);
	}
}

Graph::~Graph(){

}

int Graph::V(){
	return _V;
}

int Graph::E(){
	return _E;
}

void Graph::validateEdge(FlowEdge& edge) {
	validateVertex(edge.from());
	validateVertex(edge.to());
}

void Graph::validateVertex(int vertex){
	if(vertex < 1 || vertex > _V){
		throw std::invalid_argument("vertex number must be within 1 and " + _V);
	}
}

void Graph::addEdge(FlowEdge &edge) {
	validateEdge(edge);
	_E++;
	_adj[edge.from()][edge.to()].push_back(edge);
}

std::map<int, std::vector<FlowEdge> >& Graph::adj(int vertex){
	validateVertex(vertex);
	return _adj[vertex];
}

void Graph::setV(int V){
	validateVertex(V);
	_V = V;	
	_adj.resize(_V);
}

void Graph::clear(){
	_adj.clear();
	_V = 0;
	_E = 0;
}

std::string Graph::toString(){
	std::stringstream ss;
	for(int i = 1;i<=_V;++i){
		ss<<i<<std::endl;
		for(unsigned j = 0; j<_adj[i].size();++j){
			for(unsigned k = 0;k<_adj[i][k].size();++k){
				ss<<_adj[i][j][k].to()<<"("<<_adj[i][j][k].capacity()<<")"<<std::endl;
			}
		}
	}
	return ss.str();
}

