#ifndef STEINER_TREE_H
#define STEINER_TREE_H

#include <vector>
#include <iostream>
#include <map>
#include "SteinerTrees.h"

#include <scip/scip.h>
#include <scip/scipdefplugins.h>

namespace steiner{
	class SteinerTreeSolver{
		private:
			SCIP* _scip;
			SteinerTrees* st;
			std::vector<int> _root;
			std::vector<std::map<int, std::map<int, std::vector<SCIP_VAR *> > > > _x;
			std::vector<std::vector<SCIP_VAR *> > _u;
			std::vector<SCIP_CONS *> _cons;
		public:
			SteinerTreeSolver(char* filename);
			~SteinerTreeSolver();
			void solve(void);
			void disp(std::ostream & out = std::cout);
	};
}
#endif
