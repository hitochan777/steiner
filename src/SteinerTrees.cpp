#include "SteinerTrees.h"

SteinerTrees::SteinerTrees(int V, int t):_T(t){
	if(_T < 0){
		throw std::invalid_argument("Number of Steiner tree must be > 0");
	}
	graph = Graph(V);
}

SteinerTrees::SteinerTrees(char* filename){	
	std::ifstream ifs; 
	int V,E;
	ifs.open(filename,std::ios::in);
	if(ifs.fail()){
		throw std::invalid_argument("Failed to open "+std::string(filename));
	}
	ifs >> V >> E;
	graph = Graph(V);
	for(int i = 0,v,w,c;i<E;++i){
		ifs >> v >> w >> c;
		FlowEdge f1(v,w,c);
		FlowEdge f2(w,v,c);
		graph.addEdge(f1);
		graph.addEdge(f2);
	}
	ifs >> _T;
	if(_T <= 0){
		throw std::invalid_argument("Invalid number of steier trees");
	}
	_terminals.resize(_T);
	_bw.resize(_T);
	for(int i = 0, t, bw; i < _T; ++i){
		ifs >> t;
		for(int j = 0,v;j < t;++j){
			ifs >> v;
			addTerminal(i,v);	
		}
		ifs >> bw;
		if(bw <= 0){
			throw std::invalid_argument("bandwidth must be >= 1");
		}
		_bw[i] = bw;
	}

}

int SteinerTrees::T(){
	return _T;
}

void SteinerTrees::validateTreeNumber(int tree){
	if(tree < 0 || tree >= _T){
		throw std::invalid_argument("tree number must be within 1 and " + _T);
	}
}

SteinerTrees::~SteinerTrees(){}

void SteinerTrees::addTerminal(int tree, int terminal){
	validateTreeNumber(tree);
	graph.validateVertex(terminal);
	_terminals[tree].insert(terminal);
}

std::string SteinerTrees::toString(){
	std::stringstream ss;
	ss<<graph.toString()<<std::endl;
	for(int i = 0; i < _T;++i){
		ss<<i+1<<": "<<std::endl;
		ss<<"terminals: ";
		for(std::set<int>::iterator it = _terminals[i].begin();it!=_terminals[i].end();++it){
			ss<<*it<<" ";
		}	
		ss<<std::endl;
		ss<<"bandwidth: "<<_bw[i]<<std::endl;
	}
	return ss.str();
}

int SteinerTrees::getBandWidth(int tree){
	validateTreeNumber(tree);
	return _bw[tree];
}

std::set<int>& SteinerTrees::getTerminals(int tree){
	validateTreeNumber(tree);
	return _terminals[tree]; 
}
