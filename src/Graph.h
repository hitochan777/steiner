#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <vector>
#include <string>
#include <stdexcept>
#include <fstream>
#include <sstream>
#include <map>
#include "FlowEdge.h"

class Graph{
	protected:
		int _V;
		int _E;
		std::vector<std::map<int,std::vector<FlowEdge> > > _adj;
	public:
		void validateEdge(FlowEdge &edge); 
		void validateVertex(int vertex);
		Graph();
		Graph(int V);
		Graph(char* filename);
		~Graph();
		void addEdge(FlowEdge &edge );
		int E();
		int V();
		void clear();
		void setV(int V);
		std::map<int, std::vector<FlowEdge> >& adj(int vertex);
		std::string toString();
};

#endif
