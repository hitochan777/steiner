#!/usr/bin/env zsh

NUM=5
GENERATOR=./bin/GraphGenerator.py
DATA_DIR=./data
OUTPUT=success_rate
DELIM="\t"
mkdir -p ${DATA_DIR}

types=("cu" "cr" "r0.5u" "r0.5r" "mu" "mr")

rm $OUTPUT

for tp in "${types[@]}"
do
	for v in {2..12}
	do
		n=$(($v**2))
		for t in {2..14..2}
		do
			cnt=0
			for f in result/output.${tp}_${n}_${t}_*
			do
				if [[ `tail -1 $f` != "" ]]
				then
					cnt=$((cnt+1))
				else
				fi
			done
			echo "$tp$DELIM$n$DELIM$t$DELIM$cnt/$NUM$DELIM$(($cnt*100/$NUM))" >> $OUTPUT
		done
	done
done

echo "DONE!!"
