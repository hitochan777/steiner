#!/usr/bin/env python3
import sys
import argparse
import math
import numpy as np
# import matplotlib as mpl
# import matplotlib.pyplot as plt
import random
import gmpy

capmax = 1000
capmin = 1

def graphgen(n, tp):
    if tp=="cu":
        cap = random.randint(capmin,capmax)
        G = np.ones((n,n),dtype=int)
        G = cap*G
        for i in range(0,n):
            G[i,i] = 0
        return G
    elif tp == "cr":
        G = np.random.randint(capmin,capmax+1,size=(n,n))
        for i in range(0,n):
            G[i,i] = 0
        return G
    elif tp == "r0.5u":
        cap = random.randint(capmin,capmax)
        G = np.random.randint(0,2,size=(n,n))
        G = cap*G
        for i in range(0,n):
            G[i,i] = 0
        return G
    elif tp =="r0.5r":
        G = np.random.randint(capmin,capmax+1,size=(n,n))
        for i in range(0,n):
            G[i,i] = 0
        return G
    elif tp=="mu" or tp == "mr":
        if not gmpy.is_square(n):
            sys.exit("# of vertices must be square number.")
        sq = math.sqrt(n)
        if tp=="mu":
            cap = random.randint(capmin,capmax)
            G = np.zeros((n,n),dtype=int)
            for i in range(0,n):
                if i + sq < n:
                    G[i,i+sq] = cap
                if (i+1)%sq:
                    G[i,i+1] = cap
            return G
        else:
            G = np.zeros((n,n),dtype=int)
            for i in range(0,n):
                if i + sq < n:
                    G[i,i+sq] = random.randint(capmin,capmax)
                if (i+1)%sq:
                    G[i,i+1] = random.randint(capmin,capmax)
            return G
    else:
        sys.exit("type "+tp+" is not supported in this program.")

    G = np.random.randint(1,1000,size=(n,n))
    return G

def subsetgen(n,v,mins,maxs,minbw,maxbw):
    req = []
    bw = []
    s = []
    vertices = list(range(1,v+1))
    for i in range(0,n):
        bw.append(random.randint(minbw,maxbw))
    for i in range(0,n):
        s.append(random.randint(mins,maxs))
    for i in range(0,n):
        req.append((sorted(random.sample(vertices,s[i])),bw[i]))
    return req

def printdata(g,req, n):
    edgeCnt = 0
    for i in range(0,n):
        for j in range(i,n):
            if g[i,j]:
                edgeCnt+=1
    print(n, edgeCnt)
    for i in range(0,n):
        for j in range(i,n):
            if g[i,j]:
                print(i+1, j+1, g[i,j])
    print(len(req))
    for i in range(0,len(req)):
        bw = req[i][1]
        v = req[i][0]
        print(len(v))
        for j in range(0,len(v)):
            print(v[j],end=" ")            
        print(bw)
    return

parser = argparse.ArgumentParser()
parser.add_argument("-v",required=True,help="Number of verteces to be generated",type=int)
parser.add_argument("-t",required=True,help="Number of subsets to be generated",type=int)
parser.add_argument("--type",required=True,help="type of graph[cu|cr|r0.5u|r0.5|mu|mr]",type=str)

args = parser.parse_args()

# Number of vertices to be generated
v = args.v
# Number of steiner set to be generated
T = args.t

graphType = args.type

g = graphgen(v,graphType)

maxbw = capmax//T
minbw = 1
maxs = v
mins= 2

data = subsetgen(T,v,mins,maxs,minbw,maxbw) 

printdata(g,data,v)
